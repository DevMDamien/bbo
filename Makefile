TAG=$(shell basename $(PWD))

tests: phpstan php-cs-fixer phpunit

build:
	docker build -t $(TAG) . -f ./Dockerfile

install:
	docker run -t --rm -u $(id -u):$(id -g) -v "$$PWD":/var/task -w /var/task $(TAG) composer install
	docker run -t --rm -u $(id -u):$(id -g) -v "$$PWD":/var/task -w /var/task $(TAG) php vendor/bin/simple-phpunit install

up:
	docker-compose up -d

cli:
	docker-compose exec cli sh

phpstan:
	docker run -t --rm -u $(id -u):$(id -g) -v "$$PWD":/var/task -w /var/task $(TAG) php vendor/bin/phpstan analyse

php-cs-fixer:
	docker run -t --rm -u $(id -u):$(id -g) -v "$$PWD":/var/task -w /var/task $(TAG) php vendor/bin/php-cs-fixer fix --dry-run --diff

phpunit:
	docker run -t --rm -u $(id -u):$(id -g) -v "$$PWD":/var/task -w /var/task $(TAG) php vendor/bin/simple-phpunit

phpunit-with-coverage:
	docker run -t --rm -u $(id -u):$(id -g) -v "$$PWD":/var/task -w /var/task -e XDEBUG_MODE=coverage $(TAG) php vendor/bin/simple-phpunit --coverage-text --colors=never
