FROM bref/php-80-fpm-dev

COPY --from=bref/extra-xdebug-php-80 /opt /opt

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
ENV COMPOSER_ALLOW_SUPERUSER=1
RUN set -eux; \
	composer global require "symfony/flex" --prefer-dist --no-progress --classmap-authoritative; \
	composer clear-cache
ENV PATH="${PATH}:/root/.composer/vendor/bin"

ARG APP_ENV=dev

COPY composer.json composer.lock symfony.lock ./
RUN set -eux; \
	composer install --prefer-dist --no-scripts --no-progress --optimize-autoloader; \
	composer clear-cache

COPY bin bin/
COPY config config/
COPY public public/
COPY src src/
## only for tests
COPY tests tests/
#COPY phpunit.xml.dist .
#COPY .php-cs-fixer.dist.php .
COPY .env .
#COPY .env.test .

RUN set -eux; \
	mkdir -p /tmp/log; \
	chown nobody:nobody -Rvf /tmp; \
	composer run-script post-install-cmd; \
	chmod +x bin/console; sync
