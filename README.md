Prerequisite
===
- make
- docker
- shell (tested on linux terminal)

How to test the project
===
To prepare the project:
```shell
make build # build the docker image
make install # install dependencies in the local directory
make up # init and start the docker-compose stack
```

To run a game:
```shell
docker-compose exec cli bin/console game:start
```
After taping this command, the program will ask for the trump color, the position of the player to start, and also plays for each tricks
At the end, it will prompt a summary of the game, and display the winning team.

If you want to run test, you can also use the following commands:
```shell
make phpunit
make phpstan
make php-cs-fixer

make tests # to execute all the three commands above
```

You can also check for the code coverage with this following command:
```shell
make phpunit-with-coverage
```

Content that helped me to understand rules
===
- [Youtube - [Minute Facile] Quelles sont les règles de base du bridge sans atout](https://www.youtube.com/watch?v=WuAlh_2sEFw)
- [Youtube - [Minute Facile] Comment se déroule une partie de bridge à l'atout](https://www.youtube.com/watch?v=GPzCAACxOaU)