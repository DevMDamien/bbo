<?php

namespace App\Tests\Domain\Team;

use App\Domain\Player\PlayerPositionEnum;
use App\Domain\Team\TeamTypeEnum;
use PHPUnit\Framework\TestCase;

class TeamTypeEnumTest extends TestCase
{
    public function testAll(): void
    {
        static::assertCount(2, TeamTypeEnum::all());

        static::assertContains(TeamTypeEnum::NS, TeamTypeEnum::all());
        static::assertContains(TeamTypeEnum::EW, TeamTypeEnum::all());
    }

    public function testGetTeamByPlayerPosition(): void
    {
        static::assertSame(TeamTypeEnum::NS, TeamTypeEnum::getTeamByPlayerPosition(PlayerPositionEnum::NORTH));
        static::assertSame(TeamTypeEnum::EW, TeamTypeEnum::getTeamByPlayerPosition(PlayerPositionEnum::EAST));

        static::assertSame(TeamTypeEnum::getTeamByPlayerPosition(PlayerPositionEnum::NORTH), TeamTypeEnum::getTeamByPlayerPosition(PlayerPositionEnum::SOUTH));
        static::assertSame(TeamTypeEnum::getTeamByPlayerPosition(PlayerPositionEnum::EAST), TeamTypeEnum::getTeamByPlayerPosition(PlayerPositionEnum::WEST));
    }
}
