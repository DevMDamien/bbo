<?php

namespace App\Tests\Domain\Card;

use App\Domain\Card\CardColorEnum;
use PHPUnit\Framework\TestCase;

class CardColorEnumTest extends TestCase
{
    public function testAll(): void
    {
        static::assertCount(4, CardColorEnum::all());

        static::assertContains(CardColorEnum::DIAMOND, CardColorEnum::all());
        static::assertContains(CardColorEnum::HEARTH, CardColorEnum::all());
        static::assertContains(CardColorEnum::SPADE, CardColorEnum::all());
        static::assertContains(CardColorEnum::CLUB, CardColorEnum::all());
    }

    public function testAssertColorIsValid(): void
    {
        // no exception for this
        CardColorEnum::assertColorIsValid(CardColorEnum::DIAMOND);
        CardColorEnum::assertColorIsValid(CardColorEnum::HEARTH);
        CardColorEnum::assertColorIsValid(CardColorEnum::SPADE);
        CardColorEnum::assertColorIsValid(CardColorEnum::CLUB);

        static::expectException(\Exception::class);
        static::expectExceptionMessage('The color must be one of ♠ ♥ ♦ ♣ (see App\Domain\Card\CardColorEnum)');

        CardColorEnum::assertColorIsValid('wrong color');
    }
}
