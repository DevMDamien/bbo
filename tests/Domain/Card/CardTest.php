<?php

namespace App\Tests\Domain\Card;

use App\Domain\Card\Card;
use App\Domain\Card\CardColorEnum;
use App\Domain\Card\CardValueEnum;
use PHPUnit\Framework\TestCase;

class CardTest extends TestCase
{
    public function testIsEqualTo(): void
    {
        $a = new Card(CardColorEnum::CLUB, CardValueEnum::KING);
        $b = new Card(CardColorEnum::SPADE, CardValueEnum::KING);
        $c = new Card(CardColorEnum::CLUB, CardValueEnum::KING);
        $d = new Card(CardColorEnum::CLUB, CardValueEnum::ACE);

        static::assertFalse($a->isEqualTo($b));
        static::assertTrue($a->isEqualTo($c));
        static::assertFalse($a->isEqualTo($d));
    }

    public function testToString(): void
    {
        $a = new Card(CardColorEnum::CLUB, CardValueEnum::KING);
        $b = new Card(CardColorEnum::SPADE, CardValueEnum::KING);
        $c = new Card(CardColorEnum::CLUB, CardValueEnum::ACE);

        static::assertSame('♣K', $a->__toString());
        static::assertSame('♠K', $b->__toString());
        static::assertSame('♣A', $c->__toString());
    }
}
