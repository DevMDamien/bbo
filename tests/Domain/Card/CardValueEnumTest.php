<?php

namespace App\Tests\Domain\Card;

use App\Domain\Card\CardValueEnum;
use PHPUnit\Framework\TestCase;

class CardValueEnumTest extends TestCase
{
    public function testAll(): void
    {
        static::assertCount(13, CardValueEnum::all());

        static::assertContains(CardValueEnum::ACE, CardValueEnum::all());
        static::assertContains(CardValueEnum::KING, CardValueEnum::all());
        static::assertContains(CardValueEnum::QUEEN, CardValueEnum::all());
        static::assertContains(CardValueEnum::JACK, CardValueEnum::all());
        static::assertContains(CardValueEnum::V_10, CardValueEnum::all());
        static::assertContains(CardValueEnum::V_9, CardValueEnum::all());
        static::assertContains(CardValueEnum::V_8, CardValueEnum::all());
        static::assertContains(CardValueEnum::V_7, CardValueEnum::all());
        static::assertContains(CardValueEnum::V_6, CardValueEnum::all());
        static::assertContains(CardValueEnum::V_5, CardValueEnum::all());
        static::assertContains(CardValueEnum::V_4, CardValueEnum::all());
        static::assertContains(CardValueEnum::V_3, CardValueEnum::all());
        static::assertContains(CardValueEnum::V_2, CardValueEnum::all());
    }

    public function testAssertValueIsValid(): void
    {
        // no exception for this
        CardValueEnum::assertValueIsValid(CardValueEnum::ACE);
        CardValueEnum::assertValueIsValid(CardValueEnum::KING);
        CardValueEnum::assertValueIsValid(CardValueEnum::QUEEN);
        CardValueEnum::assertValueIsValid(CardValueEnum::JACK);
        CardValueEnum::assertValueIsValid(CardValueEnum::V_10);
        CardValueEnum::assertValueIsValid(CardValueEnum::V_9);
        CardValueEnum::assertValueIsValid(CardValueEnum::V_8);
        CardValueEnum::assertValueIsValid(CardValueEnum::V_7);
        CardValueEnum::assertValueIsValid(CardValueEnum::V_6);
        CardValueEnum::assertValueIsValid(CardValueEnum::V_5);
        CardValueEnum::assertValueIsValid(CardValueEnum::V_4);
        CardValueEnum::assertValueIsValid(CardValueEnum::V_3);
        CardValueEnum::assertValueIsValid(CardValueEnum::V_2);

        static::expectException(\Exception::class);
        static::expectExceptionMessage('The value must be one of A K Q J 10 9 8 7 6 5 4 3 2 (see App\Domain\Card\CardValueEnum)');

        CardValueEnum::assertValueIsValid('wrong value');
    }

    public function testCompare(): void
    {
        static::assertSame(0, CardValueEnum::compare(CardValueEnum::ACE, CardValueEnum::ACE));
        static::assertSame(-1, CardValueEnum::compare(CardValueEnum::ACE, CardValueEnum::KING));
        static::assertSame(1, CardValueEnum::compare(CardValueEnum::KING, CardValueEnum::ACE));
    }
}
