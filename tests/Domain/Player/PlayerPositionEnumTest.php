<?php

namespace App\Tests\Domain\Player;

use App\Domain\Player\PlayerPositionEnum;
use App\Domain\Team\TeamTypeEnum;
use PHPUnit\Framework\TestCase;

class PlayerPositionEnumTest extends TestCase
{
    public function testAll(): void
    {
        static::assertCount(4, PlayerPositionEnum::all());

        static::assertContains(PlayerPositionEnum::NORTH, PlayerPositionEnum::all());
        static::assertContains(PlayerPositionEnum::EAST, PlayerPositionEnum::all());
        static::assertContains(PlayerPositionEnum::SOUTH, PlayerPositionEnum::all());
        static::assertContains(PlayerPositionEnum::WEST, PlayerPositionEnum::all());
    }

    public function testAssertPositionIsValid(): void
    {
        // no exception for this
        PlayerPositionEnum::assertPositionIsValid(PlayerPositionEnum::NORTH);
        PlayerPositionEnum::assertPositionIsValid(PlayerPositionEnum::EAST);
        PlayerPositionEnum::assertPositionIsValid(PlayerPositionEnum::SOUTH);
        PlayerPositionEnum::assertPositionIsValid(PlayerPositionEnum::WEST);

        static::expectException(\Exception::class);
        static::expectExceptionMessage('The position must be one of N E S W (see App\Domain\Player\PlayerPositionEnum)');

        PlayerPositionEnum::assertPositionIsValid('wrong position');
    }

    public function testGetPartner(): void
    {
        static::assertSame(PlayerPositionEnum::SOUTH, PlayerPositionEnum::getPartner(PlayerPositionEnum::NORTH));
        static::assertSame(PlayerPositionEnum::WEST, PlayerPositionEnum::getPartner(PlayerPositionEnum::EAST));
        static::assertSame(PlayerPositionEnum::NORTH, PlayerPositionEnum::getPartner(PlayerPositionEnum::SOUTH));
        static::assertSame(PlayerPositionEnum::EAST, PlayerPositionEnum::getPartner(PlayerPositionEnum::WEST));

        // the exception from PlayerPositionEnum::assertPositionIsValid
        static::expectException(\Exception::class);

        PlayerPositionEnum::getPartner('wrong position');
    }

    public function testGetOpponentTeam(): void
    {
        static::assertSame(TeamTypeEnum::EW, PlayerPositionEnum::getOpponentTeam(PlayerPositionEnum::NORTH));
        static::assertSame(TeamTypeEnum::NS, PlayerPositionEnum::getOpponentTeam(PlayerPositionEnum::EAST));

        static::assertSame(PlayerPositionEnum::getOpponentTeam(PlayerPositionEnum::NORTH), PlayerPositionEnum::getOpponentTeam(PlayerPositionEnum::SOUTH));
        static::assertSame(PlayerPositionEnum::getOpponentTeam(PlayerPositionEnum::EAST), PlayerPositionEnum::getOpponentTeam(PlayerPositionEnum::WEST));

        // the exception from PlayerPositionEnum::assertPositionIsValid
        static::expectException(\Exception::class);

        PlayerPositionEnum::getPartner('wrong position');
    }

    public function testGetPlayOrder(): void
    {
        static::assertSame('NESW', PlayerPositionEnum::getPlayOrder());
    }

    public function testGetNextPlayer(): void
    {
        static::assertSame(PlayerPositionEnum::NORTH, PlayerPositionEnum::getNextPlayer(PlayerPositionEnum::WEST));
        static::assertSame(PlayerPositionEnum::EAST, PlayerPositionEnum::getNextPlayer(PlayerPositionEnum::NORTH));
        static::assertSame(PlayerPositionEnum::SOUTH, PlayerPositionEnum::getNextPlayer(PlayerPositionEnum::EAST));
        static::assertSame(PlayerPositionEnum::WEST, PlayerPositionEnum::getNextPlayer(PlayerPositionEnum::SOUTH));

        static::expectException(\Exception::class);
        static::expectExceptionMessage('The position must be one of N E S W (see App\Domain\Player\PlayerPositionEnum)');

        PlayerPositionEnum::getNextPlayer('wrong position');
    }
}
