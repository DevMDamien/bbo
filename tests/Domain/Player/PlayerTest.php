<?php

namespace App\Tests\Domain\Player;

use App\Domain\Card\Card;
use App\Domain\Card\CardColorEnum;
use App\Domain\Card\CardValueEnum;
use App\Domain\Player\Player;
use App\Domain\Player\PlayerPositionEnum;
use PHPUnit\Framework\TestCase;

class PlayerTest extends TestCase
{
    public function testDeliverCard(): void
    {
        $player = new Player(PlayerPositionEnum::NORTH);
        $card = new Card(CardColorEnum::SPADE, CardValueEnum::ACE);

        static::assertEmpty($player->getCardsByColor());
        static::assertSame(0, $player->getHandLength());
        static::assertSame(0, $player->getHandLength($card->color));

        $player->deliverCard($card);

        static::assertNotEmpty($player->getCardsByColor());
        static::assertSame(1, $player->getHandLength());
        static::assertSame(1, $player->getHandLength($card->color));
    }

    public function testAssertHasCard(): void
    {
        $player = new Player(PlayerPositionEnum::NORTH);
        $card = new Card(CardColorEnum::SPADE, CardValueEnum::ACE);

        $player->deliverCard($card);

        // no exception if the player has the card
        $player->assertHasCard($card);

        $player = new Player(PlayerPositionEnum::NORTH);

        static::expectException(\Exception::class);
        static::expectExceptionMessage('The player N does not have the card ♠A');

        $player->assertHasCard($card);
    }

    public function testRemoveCard(): void
    {
        $player = new Player(PlayerPositionEnum::NORTH);
        $card = new Card(CardColorEnum::SPADE, CardValueEnum::ACE);

        $player->deliverCard($card);

        static::assertNotEmpty($player->getCardsByColor());
        static::assertSame(1, $player->getHandLength());
        static::assertSame(1, $player->getHandLength($card->color));

        $player->removeCard($card);

        static::assertNotEmpty($player->getCardsByColor()); // because the key is not removed
        static::assertSame(0, $player->getHandLength());
        static::assertSame(0, $player->getHandLength($card->color));
    }

    public function testGetCardsByColor(): void
    {
        $player = new Player(PlayerPositionEnum::NORTH);
        $card = new Card(CardColorEnum::SPADE, CardValueEnum::ACE);

        static::assertSame([], $player->getCardsByColor());

        $player->deliverCard($card);

        static::assertSame([$card->color => [$card]], $player->getCardsByColor());
    }
}
