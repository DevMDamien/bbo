<?php

namespace App\Tests\Domain\Trick;

use App\Domain\Game\Game;
use App\Domain\Player\PlayerPositionEnum;
use PHPUnit\Framework\TestCase;

class TrickTest extends TestCase
{
    public function testGetCurrentPlayPosition(): void
    {
        $game = new Game(null, PlayerPositionEnum::NORTH);
        $trick = $game->nextTrick();

        static::assertSame($game->startPosition, $trick->getCurrentPlayPosition());
    }

    public function testGetColor(): void
    {
        $game = new Game(null, PlayerPositionEnum::NORTH);
        $trick = $game->nextTrick();

        static::assertNull($trick->getColor());

        $player = $game->getPlayer($game->startPosition);
        $card = $player->getPlayableCards($trick)[0];

        $trick->play($player, $card);

        static::assertSame($card->color, $trick->getColor());
    }

    public function testPlay(): void
    {
        $game = new Game(null, PlayerPositionEnum::NORTH);
        $trick = $game->nextTrick();

        $player = $game->getPlayer($game->startPosition);
        $card = $player->getPlayableCards($trick)[0];

        // no exception
        $player->assertHasCard($card);

        $trick->play($player, $card);

        static::expectException(\Exception::class);
        static::expectExceptionMessageMatches('/The player N does not have the card .+/m');

        $player->assertHasCard($card);
    }
}
