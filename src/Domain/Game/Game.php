<?php

namespace App\Domain\Game;

use App\Domain\Card\Card;
use App\Domain\Card\CardColorEnum;
use App\Domain\Card\CardValueEnum;
use App\Domain\Player\Player;
use App\Domain\Player\PlayerPositionEnum;
use App\Domain\Team\TeamTypeEnum;
use App\Domain\Trick\Trick;

class Game
{
    private bool $initialized = false;

    // TODO: remove the field ?
    private array $cards;
    /** @var array<PlayerPositionEnum::*, Player> */
    private array $players;
    /** @var array<Trick> */
    private array $tricks = [];
    private ?Trick $currentTrick = null;

    public function __construct(
        public ?string $trumpColor,
        public string $startPosition,
    ) {
        if (null !== $this->trumpColor) {
            CardColorEnum::assertColorIsValid($this->trumpColor);
        }

        PlayerPositionEnum::assertPositionIsValid($this->startPosition);

        $this->init();
    }

    public function init(): void
    {
        if ($this->initialized) {
            return;
        }

        // we generate the cards
        $this->cards = [];
        foreach (CardColorEnum::all() as $cardColor) {
            foreach (CardValueEnum::all() as $cardValue) {
                $this->cards[] = new Card($cardColor, $cardValue);
            }
        }

        // we shuffle the cards
        shuffle($this->cards);

        // we generate players
        foreach (PlayerPositionEnum::all() as $playerPosition) {
            $this->players[$playerPosition] = new Player($playerPosition);
        }

        // we distribute cards
        $this->distributeCards($this->startPosition);

        $this->initialized = true;
    }

    /**
     * Distribute each cards to the player.
     *
     * We begin to distribute to the first player to play.
     */
    public function distributeCards(string $startPosition): void
    {
        $currentPosition = $startPosition;
        for ($i = 0; $i < \count($this->cards); ++$i) {
            $player = $this->getPlayer($currentPosition);
            $player->deliverCard($this->cards[$i]);

            $currentPosition = PlayerPositionEnum::getNextPlayer($currentPosition);
        }
    }

    public function getPlayer(string $position): Player
    {
        PlayerPositionEnum::assertPositionIsValid($position);

        return $this->players[$position];
    }

    public function nextTrick(): Trick
    {
        // TODO: check that the game has been initialized
        return $this->tricks[] = $this->currentTrick = new Trick(
            $this,
            ($this->currentTrick?->number ?? 0) + 1,
            $this->currentTrick?->getWinner() ?? $this->startPosition
        );
    }

    /** @return array<Trick> */
    public function getTricks(): array
    {
        return $this->tricks;
    }

    public function isFinished(): bool
    {
        return 13 === \count($this->tricks);
    }

    public function getResult(): array
    {
        $count = [];
        foreach (TeamTypeEnum::all() as $team) {
            $count[$team] = 0;
        }

        foreach ($this->tricks as $trick) {
            if (!$trick->everybodyPlayed()) {
                continue;
            }

            ++$count[TeamTypeEnum::getTeamByPlayerPosition($trick->getWinner())];
        }

        return $count;
    }
}
