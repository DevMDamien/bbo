<?php

namespace App\Domain\Player;

use App\Domain\Card\Card;
use App\Domain\Card\CardColorEnum;
use App\Domain\Card\CardValueEnum;
use App\Domain\Trick\Trick;

class Player
{
    private array $cards = [];
    /** Cards grouped by color, sort by value */
    private array $cardsByColor = [];

    public function __construct(
        public string $position,
    ) {
        PlayerPositionEnum::assertPositionIsValid($this->position);
    }

    public function deliverCard(Card $card): void
    {
        $this->cards[] = $card;

        if (!\array_key_exists($card->color, $this->cardsByColor)) {
            $this->cardsByColor[$card->color] = [];
        }

        $this->cardsByColor[$card->color][] = $card;

        usort($this->cardsByColor[$card->color], fn (Card $left, Card $right) => CardValueEnum::compare($left->value, $right->value));
    }

    public function assertHasCard(Card $card): void
    {
        $cardPosition = array_search($card, $this->cards, true);
        if (false !== $cardPosition) {
            return;
        }

        throw new \Exception(sprintf('The player %s does not have the card %s', $this, $card));
    }

    public function removeCard(Card $card): void
    {
        $this->assertHasCard($card);

        $cardByColorPosition = array_search($card, $this->cardsByColor[$card->color], true);
        $cardPosition = array_search($card, $this->cards, true);

        unset($this->cardsByColor[$card->color][$cardByColorPosition]);
        unset($this->cards[$cardPosition]);
    }

    public function getCardsByColor(): array
    {
        return $this->cardsByColor;
    }

    /** @return array<Card> */
    public function getPlayableCards(Trick $trick): array
    {
        $cards = null;
        // if the trick already have a card played (and then a color defined), we select the player cards with the same color
        if (null !== $trick->getColor() && 0 !== $this->getHandLength($trick->getColor())) {
            $cards[$trick->getColor()] = $this->getCardsByColor()[$trick->getColor()];
        }

        // if $cards is null (trick without any color (first player to play), or does not have the possibility to play the same color), the player can play any card from his hand
        if (null === $cards) {
            $cards = $this->getCardsByColor();
        }

        return array_merge(...array_values($cards));
    }

    public function getHandLength(?string $cardColor = null): int
    {
        if (null === $cardColor) {
            return \count($this->cards);
        }

        CardColorEnum::assertColorIsValid($cardColor);

        return \count($this->cardsByColor[$cardColor] ?? []);
    }

    public function __toString(): string
    {
        return $this->position;
    }
}
