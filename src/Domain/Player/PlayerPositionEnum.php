<?php

namespace App\Domain\Player;

use App\Domain\Team\TeamTypeEnum;

class PlayerPositionEnum
{
    public const NORTH = 'N';
    public const SOUTH = 'S';
    public const EAST = 'E';
    public const WEST = 'W';

    /**
     * Get all the available positions for a player.
     */
    public static function all(): array
    {
        return [static::NORTH, static::EAST, static::SOUTH, static::WEST];
    }

    /**
     * Assert that the position in parameter is a valid position.
     */
    public static function assertPositionIsValid(string $position): void
    {
        if (!\in_array($position, static::all(), true)) {
            throw new \Exception(sprintf('The position must be one of %s (see %s)', implode(' ', static::all()), static::class));
        }
    }

    public static function getPartner(string $position): string
    {
        static::assertPositionIsValid($position);

        return match ($position) {
            static::NORTH => static::SOUTH,
            static::EAST  => static::WEST,
            static::SOUTH => static::NORTH,
            static::WEST  => static::EAST,
            default       => new \LogicException(),
        };
    }

    public static function getOpponentTeam(string $position): string
    {
        static::assertPositionIsValid($position);

        return match ($position) {
            static::NORTH, static::SOUTH => TeamTypeEnum::EW,
            static::EAST, static::WEST => TeamTypeEnum::NS,
            default => new \LogicException(),
        };
    }

    /**
     * Ge the play order.
     */
    public static function getPlayOrder(): string
    {
        return implode('', static::all());
    }

    /**
     * Get the next player to play.
     */
    public static function getNextPlayer(string $currentPlayer): string
    {
        static::assertPositionIsValid($currentPlayer);

        $playOrder = sprintf('%1$s%1$s', static::getPlayOrder());

        $position = mb_strpos($playOrder, $currentPlayer);
        $nextPlayer = $playOrder[$position + 1];

        static::assertPositionIsValid($nextPlayer);

        return $nextPlayer;
    }
}
