<?php

namespace App\Domain\Team;

use App\Domain\Player\PlayerPositionEnum;

class TeamTypeEnum
{
    public const NS = 'NS';
    public const EW = 'EW';

    /**
     * Get all the available positions for a player.
     */
    public static function all(): array
    {
        return [static::NS, static::EW];
    }

    public static function getTeamByPlayerPosition(string $playerPosition): string
    {
        PlayerPositionEnum::assertPositionIsValid($playerPosition);

        return match ($playerPosition) {
            PlayerPositionEnum::NORTH, PlayerPositionEnum::SOUTH => static::NS,
            PlayerPositionEnum::EAST, PlayerPositionEnum::WEST => static::EW,
            default => throw new \LogicException()
        };
    }
}
