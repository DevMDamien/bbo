<?php

namespace App\Domain\Trick;

use App\Domain\Card\Card;
use App\Domain\Card\CardValueEnum;
use App\Domain\Game\Game;
use App\Domain\Player\Player;
use App\Domain\Player\PlayerPositionEnum;

class Trick
{
    /** The position of the player to play */
    private string $currentPlayPosition;
    private ?string $color = null;

    /**
     * Cards played by players.
     *
     * @var array<string|PlayerPositionEnum::*, Card>
     */
    private array $playedCards = [];
    private ?string $winner = null;

    public function __construct(
        public Game $game,
        public int $number,
        public string $startPosition,
    ) {
        PlayerPositionEnum::assertPositionIsValid($this->startPosition);

        $this->currentPlayPosition = $this->startPosition;
    }

    public function getCurrentPlayPosition(): string
    {
        return $this->currentPlayPosition;
    }

    /**
     * Get the color of the Trick (the color of the first card played).
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    public function play(Player $player, Card $card): void
    {
        if ($player->position !== $this->currentPlayPosition) {
            throw new \Exception(sprintf('It is not the turn of the player %s', $player));
        }

        $player->assertHasCard($card);

        $playableCards = $player->getPlayableCards($this);
        if (!\in_array($card, $playableCards, true)) {
            throw new \Exception('The player could not play this card');
        }

        $this->playedCards[$player->position] = $card;

        $player->removeCard($card);

        if (null === $this->color) {
            $this->color = $card->color;
        }

        $this->currentPlayPosition = PlayerPositionEnum::getNextPlayer($this->currentPlayPosition);
    }

    public function everybodyPlayed(): bool
    {
        return 4 === \count($this->playedCards);
    }

    public function getWinner(): string
    {
        if (!$this->everybodyPlayed()) {
            throw new \Exception('The trick is not finished');
        }

        if (null !== $this->winner) {
            return $this->winner;
        }

        /** @var Card|null $currentCard */
        $currentCard = null;
        $winner = null;

        /** @var Card $card */
        foreach ($this->playedCards as $playerPosition => $card) {
            if (null === $winner) {
                $winner = $playerPosition;
                $currentCard = $card;

                continue;
            }

            if ($currentCard->color === $card->color && 1 === CardValueEnum::compare($currentCard->value, $card->value)) {
                $winner = $playerPosition;
                $currentCard = $card;

                continue;
            }

            if ($currentCard->color !== $card->color && $this->game->trumpColor === $card->color) {
                $winner = $playerPosition;
                $currentCard = $card;

                continue;
            }
        }

        return $this->winner = $winner;
    }

    public function __toString(): string
    {
        $parts = [];
        foreach ($this->playedCards as $player => $card) {
            $parts[] = sprintf('%s %s', $player, $card);
        }

        return implode(' | ', $parts);
    }
}
