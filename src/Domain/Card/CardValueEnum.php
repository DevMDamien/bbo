<?php

namespace App\Domain\Card;

class CardValueEnum
{
    public const ACE = 'A';
    public const KING = 'K';
    public const QUEEN = 'Q';
    public const JACK = 'J';
    public const V_10 = '10';
    public const V_9 = '9';
    public const V_8 = '8';
    public const V_7 = '7';
    public const V_6 = '6';
    public const V_5 = '5';
    public const V_4 = '4';
    public const V_3 = '3';
    public const V_2 = '2';

    /**
     * Get all the available values for a card.
     *
     * Values are sort by power (higher first).
     */
    public static function all(): array
    {
        return [
            static::ACE,
            static::KING,
            static::QUEEN,
            static::JACK,
            static::V_10,
            static::V_9,
            static::V_8,
            static::V_7,
            static::V_6,
            static::V_5,
            static::V_4,
            static::V_3,
            static::V_2,
        ];
    }

    public static function assertValueIsValid(string $color): void
    {
        if (!\in_array($color, static::all(), true)) {
            throw new \Exception(sprintf('The value must be one of %s (see %s)', implode(' ', static::all()), static::class));
        }
    }

    /**
     * Compare two card values.
     *
     * If the two values are equal, returns 0
     * If the left value is greater than the right value, return -1
     * Else return 1
     *
     * @see https://www.php.net/manual/fr/function.usort.php
     */
    public static function compare(string $left, string $right): int
    {
        static::assertValueIsValid($left);
        static::assertValueIsValid($right);

        if ($left === $right) {
            return 0;
        }

        // we know the power of a value with the position in the static::all() array
        $leftPosition = array_search($left, static::all(), true);
        $rightPosition = array_search($right, static::all(), true);

        if (false === $leftPosition || false === $rightPosition) {
            throw new \LogicException(sprintf('At least one of %s %s is missing in %s::all()', $left, $right, static::class));
        }

        return $leftPosition < $rightPosition ? -1 : 1;
    }
}
