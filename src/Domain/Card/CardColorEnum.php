<?php

namespace App\Domain\Card;

class CardColorEnum
{
    public const SPADE = '♠';
    public const HEARTH = '♥';
    public const DIAMOND = '♦';
    public const CLUB = '♣';

    public static function all(): array
    {
        return [static::SPADE, static::HEARTH, static::DIAMOND, static::CLUB];
    }

    public static function assertColorIsValid(string $color): void
    {
        if (!\in_array($color, static::all(), true)) {
            throw new \Exception(sprintf('The color must be one of %s (see %s)', implode(' ', static::all()), static::class));
        }
    }
}
