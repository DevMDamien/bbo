<?php

namespace App\Domain\Card;

class Card
{
    public function __construct(
        public string $color,
        public string $value,
    ) {
        CardColorEnum::assertColorIsValid($this->color);
        CardValueEnum::assertValueIsValid($this->value);
    }

    public function isEqualTo(self $other): bool
    {
        return $this->color === $other->color && $this->value === $other->value;
    }

    public function __toString(): string
    {
        return sprintf('%s%s', $this->color, $this->value);
    }
}
