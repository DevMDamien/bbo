<?php

namespace App\Application\Console;

use App\Domain\Card\Card;
use App\Domain\Card\CardColorEnum;
use App\Domain\Game\Game;
use App\Domain\Player\Player;
use App\Domain\Player\PlayerPositionEnum;
use App\Domain\Team\TeamTypeEnum;
use App\Domain\Trick\Trick;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'game:start',
    description: 'Start a game of Bridge',
)]
class GameStartCommand extends Command
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        PlayerPositionEnum::getNextPlayer(PlayerPositionEnum::NORTH);

        $question = new ChoiceQuestion('Please select the trump color', [...CardColorEnum::all(), 'Play without trump']);
        $trumpColor = $io->askQuestion($question);

        $question = new ChoiceQuestion('Please select the first player to play', PlayerPositionEnum::all());
        $firstPlayPosition = $io->askQuestion($question);

        // game to create in a service ?
        $game = new Game(
            \in_array($trumpColor, CardColorEnum::all(), true) ? $trumpColor : null,
            $firstPlayPosition
        );

        while (!$game->isFinished()) {
            $trick = $game->nextTrick();

            $io->title(sprintf('[Trick n°%s]', $trick->number));
            while (!$trick->everybodyPlayed()) {
                $player = $game->getPlayer($trick->getCurrentPlayPosition());
                $card = $this->askCardToPlay($io, $player, $trick);

                $trick->play($player, $card);

                $io->writeln($trick);
            }

            $io->writeln(sprintf('Trick won by %s', $trick->getWinner()));

            // a little pause to let the user read the result
            sleep(2);
        }

        $rows = [];
        foreach ($game->getTricks() as $trick) {
            $rows[] = [$trick->number, $trick->__toString(), $trick->getWinner()];
        }

        $io->table(['Trick number', 'cards played', 'winner'], $rows);

        $result = $game->getResult();

        $io->newLine(2);
        $io->table([TeamTypeEnum::NS, TeamTypeEnum::EW], [[$result[TeamTypeEnum::NS], $result[TeamTypeEnum::EW]]]);
        $io->success(sprintf('Game won by %s', $result[TeamTypeEnum::NS] > $result[TeamTypeEnum::EW] ? TeamTypeEnum::NS : TeamTypeEnum::EW));

        return Command::SUCCESS;
    }

    public function displayPlayerHand(SymfonyStyle $io, Player $player): void
    {
        $io->title(sprintf('[Hand] Player %s', $player->position));
        foreach ($player->getCardsByColor() as $color => $cards) {
            $io->writeln(sprintf('%s %s', $color, implode(' ', array_column($cards, 'value'))));
        }
        $io->newLine();
    }

    // TODO: move this method, it is not linked to the application layer, but the domain
    public function askCardToPlay(SymfonyStyle $io, Player $player, Trick $trick): Card
    {
        $cards = $player->getPlayableCards($trick);

        $this->displayPlayerHand($io, $player);
        $question = new ChoiceQuestion('Please select the card to play', $cards);

        return $io->askQuestion($question);
    }
}
